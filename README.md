# castertools
Some tools for the cool casters. For help/suggestions, use GitLab Issues or email me at noel@noynoy.org.

## Installation
`pip install -r requirements`

## Running
First, create a `config.py` file with the following parameters:

```
TWITCH_CLIENT_ID = '<your_twitch_client_id>`
```

Then run: `python application.py`
