from flask_api import FlaskAPI
from flask import request
from twitch import TwitchClient
from string import Template

application = FlaskAPI(__name__)


@application.route('/', methods=['GET'])
def root():
    return "Welcome! Castertools is an open source project. For contact/more info, visit https://gitlab.com/nbau21/castertools"


@application.route('/cross', methods=['GET'])
def cross():
    from_usr = request.args.get('from_usr')
    # custom_msg MUST include $url in the message
    custom_msg = request.args.get('custom_msg')

    if from_usr is None or custom_msg is None:
        return "Cannot find from_usr. Please use the endpoint /cross?from_usr=<your username>&custom_msg=<msg> and make sure it is correct."
    if custom_msg.__contains__('$url') is not True:
        return "Please make sure your custom_msg includes $url. Then URL encode the entire custom_msg using urlencoder.org"
    application.config.from_pyfile('config.py')
    client = TwitchClient(client_id=application.config['TWITCH_CLIENT_ID'])
    channels = client.search.channels(from_usr, limit=1)
    users = from_usr

    if len(channels) == 0:
        return "Cannot find from_usr. Please use the endpoint /cross?from_usr=<your username> and make sure it is correct."
    words = channels[0].status.split()
    for word in words:
        if word.startswith("@"):
            users = users + "/" + word[1:len(word)]
    custom_msg_template = Template(custom_msg)
    return custom_msg_template.substitute(url="https://kadgar.net/live/" + users)


if __name__ == "__main__":
    # application.run(debug=True)
    application.run()
